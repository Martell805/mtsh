import model.Circle;
import model.Polygon;
import model.Rectangle;
import model.Rhomb;

import java.awt.Color;
import java.util.List;

public class Main {
    public static void test() {
        Circle circle = new Circle(5, 6, 1, Color.BLACK);
        System.out.println(circle.getArea() + " " + circle.getPerimeter());

        Rectangle rectangle = new Rectangle(5, 5, 10, 5, Color.BLACK);
        System.out.println(rectangle.getArea() + " " + rectangle.getPerimeter());

        Polygon polygon = new Polygon(List.of(0, 0, 1, 1), List.of(0, 1, 1, 0), Color.BLACK);
        System.out.println(polygon.getArea() + " " + polygon.getPerimeter());

        Rhomb rhomb = new Rhomb(0, 0, 1, 1, Color.BLACK);
        System.out.println(rhomb.getArea() + " " + rhomb.getPerimeter());
        System.out.println("Ромб - это ромб: " + rhomb.isRhomb());
    }

    public static void main(String[] args) {
        test();
    }
}
