package model;

import lombok.Getter;
import lombok.Setter;

import java.awt.Color;

@Getter
@Setter
public class Circle extends GeomFigure {
    protected int x;
    protected int y;
    protected int radius;

    public Circle(int x, int y, int radius, Color color){
        this.x = x;
        this.y = y;
        this.radius = radius;

        this.setColor(color);
    }

    @Override
    public float getArea() {
        return (float)Math.PI * radius * radius;
    }

    @Override
    public float getPerimeter() {
        return (float)Math.PI * radius * 2;
    }
}
