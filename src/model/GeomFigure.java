package model;

import lombok.Getter;
import lombok.Setter;

import java.awt.Color;

@Getter
@Setter
public abstract class GeomFigure {
    private Color color;

    public abstract float getArea();

    public abstract float getPerimeter();
}
