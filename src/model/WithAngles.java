package model;

import java.util.List;

public interface WithAngles {
    List<Integer> getXCoordinates();

    List<Integer> getYCoordinates();
}
