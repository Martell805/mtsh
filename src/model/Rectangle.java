package model;

import lombok.Getter;
import lombok.Setter;

import java.awt.Color;
import java.util.List;

@Getter
@Setter
public class Rectangle extends GeomFigure implements WithAngles{
    protected int x;
    protected int y;
    protected int width;
    protected int height;

    public Rectangle(int x, int y, int width, int height, Color color){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;

        this.setColor(color);
    }

    @Override
    public float getArea() {
        return width * height;
    }

    @Override
    public float getPerimeter() {
        return (width + height) * 2;
    }


    @Override
    public List<Integer> getXCoordinates() {
        return List.of(this.x);
    }

    @Override
    public List<Integer> getYCoordinates() {
        return List.of(this.y);
    }
}
