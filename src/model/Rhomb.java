package model;

import java.awt.Color;

public class Rhomb extends Polygon implements RhombLike{
    public Rhomb(int centerX, int centerY, int halfVerticalDiagonal, int halfHorizontalDiagonal, Color color) {
        this.addVertex(centerX, centerY + halfVerticalDiagonal);
        this.addVertex(centerX + halfHorizontalDiagonal, centerY);
        this.addVertex(centerX, centerY - halfVerticalDiagonal);
        this.addVertex(centerX - halfHorizontalDiagonal, centerY);

        this.setColor(color);
    }

    @Override
    public float getArea() {
        return (float)(this.vertexX.get(1) - this.vertexX.get(3)) * (this.vertexY.get(0) - this.vertexY.get(2)) / 2;
    }

    @Override
    public float getPerimeter() {
        return this.getEdge(0, 1) * 4;
    }

    @Override
    public boolean isRhomb() {
        return true;
    }
}
